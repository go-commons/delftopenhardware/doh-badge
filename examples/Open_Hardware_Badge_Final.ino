/*
   This code was designed to be used on the Delft Open Hardware badge in combination with a color sensor.

   Steps to operate:
      1. Select the correct board "Generic ESP8266 Module". This can be found under Tools > Board: > ESP8266 Boards
      3. Select the correct port under Tools > Port. You can identify the correct one by seeing which port is added
         when plugging the badge into your comupter.
      4. Click the right arrow button (Upload) in the top left of this window to upload your code.
      5. Connect to the WiFi network "OpenHardware Badge" to configure.
*/

// LED will blink when in config mode
#include <FS.h>
#include <string.h>
#include <Adafruit_DotStar.h> // Include libraries
#include <SPI.h>

#define S2 12 // Define pins for color sensor
#define S3 10
#define sensorOut 13
int rawRed = 0, red = 0 , rawGreen = 0, green = 0, rawBlue = 0, blue = 0; // Define color variables used
float brightness = 0.2; // Maximum brightness where 0 = 0% and 1 = 100%

#define NUMPIXELS 7 // Number of LEDs in RGB strip
#define DATAPIN 14  // Define pins for RGB LEDs
#define CLOCKPIN 16
Adafruit_DotStar strip(NUMPIXELS, DATAPIN, CLOCKPIN, DOTSTAR_BRG);  // Create LED Strip

/////////////////////////////////////start code for wifimanager//////////////////////////////////////////////
#include <WiFiManager.h> // https://github.com/tzapu/WiFiManager
WiFiManagerParameter badge_name_field; // global param ( for non blocking w params )
WiFiManagerParameter mqtt_username_field; // global param ( for non blocking w params )
WiFiManagerParameter mqtt_password_field; // global param ( for non blocking w params )
WiFiManager wm;

//for LED status
#include <Ticker.h>
Ticker ticker;
#ifndef LED_BUILTIN
#endif
#define TRIGGER_PIN 5
int LED = LED_BUILTIN;
void tick() {
  //toggle state
  digitalWrite(LED, !digitalRead(LED)); // Set pin to the opposite state
}

// Gets called when WiFiManager enters configuration mode
void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());  // If you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID()); // Entered config mode, make led toggle faster
  ticker.attach(0.2, tick);
}
String getParam(String name) {
  //read parameter from server, for customhmtl input
  String value;
  if (wm.server->hasArg(name)) {
    value = wm.server->arg(name);
  }
  return value;
}
void saveParamCallback() {
  Serial.println("[CALLBACK] saveParamCallback fired");
  Serial.println("PARAM customfieldid = " + getParam("customfieldid"));
}

/////////////////////////////////////end code for wifimanager///////////////////////////////////////////////

/////////////////////////////////////start code for SAVE////////////////////////////////////////////////////
bool shouldSaveConfig = false;  // Flag for saving data

void saveConfigCallback () {    // Callback notifying us of the need to save config
  Serial.println("Should save config");
  shouldSaveConfig = true;
}
/////////////////////////////////////end code for SAVE//////////////////////////////////////////////////////

/////////////////////////////////////start code for MQTT////////////////////////////////////////////////////
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>
WiFiClient espClient;
PubSubClient client(espClient);

const long updateInterval = 5000;
unsigned long previousMillis = millis();

const char* mqttServer = "testbed.dcc.tudelft.nl";
const int mqttPort = 8080;
const char* badgeName = "";
const char* mqttUser = "";
const char* mqttPassword = "";
char mqttTopic[60] = {0};

/////////////////////////////////////end code for MQTT////////////////////////////////////////////////////////

void setup() {
  /////////////////////////////////////start setup code for SAVE//////////////////////////////////////////////
  Serial.println("mounting FS...");
  if (SPIFFS.begin()) {
    Serial.println("mounted file system");
    if (SPIFFS.exists("/config.json")) {
      //file exists, reading and loading
      Serial.println("reading config file");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);
        configFile.readBytes(buf.get(), size);
        DynamicJsonDocument doc(1024);
        DeserializationError error = deserializeJson(doc, buf.get());
        if (error)
          return;
        badgeName = doc["badgeName"];
        mqttUser = doc["mqttUser"];
        mqttPassword = doc["mqttPassword"];
        configFile.close();
      }
    }
  } else {
    Serial.println("failed to mount FS");
  }
  //end read
  /////////////////////////////////////end setup code for SAVE//////////////////////////////////////////////////

  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(sensorOut, INPUT);

  strip.begin(); // Initialize pins for LED output
  strip.show();  // Update LEDs

  Serial.begin(115200); // Initialize serial Port

  digitalWrite(5, HIGH);
  if (digitalRead(4) == 1) { // Test for Online/Offline mode

    WiFi.mode(WIFI_STA); // Explicitly set mode, esp defaults to STA+AP

    /////////////////////////////////////start setup code for wifimanager////////////////////////////////////////
    //set led pin as output
    pinMode(LED, OUTPUT);
    // start ticker with 0.5 because we start in AP mode and try to connect
    ticker.attach(0.6, tick);

    wm.setAPCallback(configModeCallback);
    int customFieldLength = 50;
    new (&badge_name_field) WiFiManagerParameter("badge_name", "Name your badge: (no special characters and use a unique name)", badgeName, customFieldLength);
    new (&mqtt_username_field) WiFiManagerParameter("username", "Username:", mqttUser, customFieldLength);
    new (&mqtt_password_field) WiFiManagerParameter("password", "Password:", mqttPassword, customFieldLength);

    wm.setSaveConfigCallback(saveConfigCallback);
    wm.addParameter(&badge_name_field);
    wm.addParameter(&mqtt_username_field);
    wm.addParameter(&mqtt_password_field);
    wm.setSaveParamsCallback(saveParamCallback);
    wm.setClass("invert");

    //wm.setTimeout(30);
    //wm.setAPClientCheck(true); // avoid timeout if client connected to softap
    //fetches ssid and pass and tries to connect
    //if it does not connect it starts an access point with the specified name
    //here  "AutoConnectAP"
    //and goes into a blocking loop awaiting configuration
    if (!wm.autoConnect("OpenHardware Badge")) {
      Serial.println("failed to connect and hit timeout");
      //reset and try again, or maybe put it to deep sleep
      ESP.restart();
      delay(1000);
    }
    //if you get here you have connected to the WiFi
    Serial.println("WiFi successfully connected");
    ticker.detach();
    //keep LED on
    digitalWrite(LED, LOW);
    /////////////////////////////////////end setup code for wifimanager///////////////////////////////////////////

    /////////////////////////////////////start setup code for MQTT////////////////////////////////////////////////
    char clientID[30];
    snprintf(clientID, 30, "ESP8266-%08X", ESP.getChipId());

    badgeName = badge_name_field.getValue();
    mqttUser = mqtt_username_field.getValue();
    mqttPassword = mqtt_password_field.getValue();
    sprintf(mqttTopic, "%s%s", "workshop/", badgeName);

    Serial.printf("\nAssigned ChipID:%s\n",clientID);
    Serial.printf("Name:%s\nAddress:%s:%d\nU:%s\nP:%s\nTopic:%s\n\n", badgeName, mqttServer, mqttPort, mqttUser, mqttPassword, mqttTopic);

    client.setServer(mqttServer, mqttPort);
    client.setCallback(callback);

    while (!client.connected()) {
      Serial.print("Connecting to MQTT...");

      if (client.connect(clientID, mqttUser, mqttPassword )) {
        Serial.println("connected");
      } else {
        Serial.print("failed with state ");
        Serial.println(client.state());
        delay(2000);
      }
    }
    client.publish(mqttTopic, "connected"); //Topic name
    client.subscribe(mqttTopic);

    /////////////////////////////////////end setup code for MQTT//////////////////////////////////////////////////

    /////////////////////////////////////start setup code for SAVE////////////////////////////////////////////////
    //save the custom parameters to FS
    if (shouldSaveConfig) {
      Serial.print("Saving config...");
      DynamicJsonDocument doc(1024);
      doc["badgeName"] = badgeName;
      doc["mqttUser"] = mqttUser;
      doc["mqttPassword"] = mqttPassword;

      File configFile = SPIFFS.open("/config.json", "w");
      if (!configFile) {
        Serial.println("failed to open config file for writing");
      }
      serializeJson(doc, configFile);
      Serial.println("successful");
      Serial.print("Config: ");
      serializeJson(doc, Serial);
      Serial.println();
      configFile.close();
    }

    /////////////////////////////////////end setup code for SAVE///////////////////////////////////////////////////


  } else {
    wm.resetSettings(); // Reset wifi credentials if in offline mode
    Serial.print("Erasing config file...");
    if (SPIFFS.remove("/config.json")) {
      Serial.println("successful");
    } else {
      Serial.println("failed");
    }
    Serial.println("OFFLINE MODE STARTUP COMPLETE");
  }
}

void callback(char* topic, byte* payload, unsigned int length) {

  Serial.print("Message arrived in topic: ");
  Serial.println(topic);

  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }

  Serial.println();

}


void loop() {


  /////////////////////////////////////start sending code for MQTT///////////////////////////////////////////////////
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= updateInterval) {
    if (digitalRead(4) == 1) { // Test for Online/Offline mode
      StaticJsonDocument<300> JSONbuffer;
      JsonObject JSONencoder = JSONbuffer.to<JsonObject>();

      JSONencoder["name"] = badgeName;
      JsonArray color = JSONencoder.createNestedArray("color");
      color.add(red);
      color.add(green);
      color.add(blue);

      char JSONmessageBuffer[100];
      serializeJson(JSONencoder, JSONmessageBuffer);
      Serial.println("Sending message to MQTT topic..");
      Serial.println(JSONmessageBuffer);

      if (client.publish(mqttTopic, JSONmessageBuffer) == true) {
        Serial.println("Success sending message");
      } else {
        Serial.println("Error sending message");
      }

      client.loop();
      Serial.println("-----------------------------------------------------");
    } else {
      Serial.printf("R: %d \t", red);
      Serial.printf("G: %d \t", green);
      Serial.printf("B: %d \n", blue);
    }

    // save the last time message was sent
    previousMillis = currentMillis;
  }
  /////////////////////////////////////end sending code for MQTT////////////////////////////////////////////////////


  // Setting Red filtered photodiodes to be read
  digitalWrite(S2, LOW); // Configure which color the diode reads
  digitalWrite(S3, LOW);

  rawRed = pulseIn(sensorOut, LOW);   // Measure amount of that color
  red = map(rawRed, 45, 600, 255, 0); // Calibrate and scale result to RGB values
  red = constrain(red, 0, 255);     // Make sure values do not exceed RGB values

  //Serial.printf("R: %d \t", red);  // Print color value to serial
  delay(5);

  // Setting Green filtered photodiodes to be read
  digitalWrite(S2, HIGH);
  digitalWrite(S3, HIGH);

  rawGreen = pulseIn(sensorOut, LOW);
  green = map(rawGreen, 50, 700, 255, 0);
  green = constrain(green, 0, 255);

  //Serial.printf("G: %d \t", green);
  delay(5);

  // Setting Blue filtered photodiodes to be read
  digitalWrite(S2, LOW);
  digitalWrite(S3, HIGH);

  rawBlue = pulseIn(sensorOut, LOW);
  blue = map(rawBlue, 40, 600, 255, 0);
  blue = constrain(blue, 0, 255);

  //Serial.printf("B: %d \n", blue);
  delay(5);

  // Writing colors to all the pixels
  for ( int i = 0; i < NUMPIXELS; i++) {
    strip.setPixelColor(i, green * brightness, red * brightness, blue * brightness); // Write color to pixels
  }

  strip.show(); // Refresh RGB strip

}
