Hi everyone✌

you are now joining the adventurous part of the project🚧


> Please note that uploading code from here disconnects your board from the workshop and you are not able to sense colors and connect to the web out of the box.

🤓 that said, lets head into the nerdy part

This folder consists of the following files: 
- blink_example_DOH_badge.ino, this file allows you to blink the lights in the colors you like!
- sense_color.ino, this file allows you to measure colors with the color sensor
- open_hardware_badge_final.ino, this file is the one used in the workshop!



in order to get these sketches to work you need to do the following things once:
- download and install the latest version of the Arduino IDE
- once opened go to file -> preferences -> and under Additional Board Managers add:
    https://arduino.esp8266.com/stable/package_esp8266com_index.json
- once added click ok to close the window
- next click Sketch -> Include Library -> Manage Library
- let it load for a few seconds
- search for Adafruit Dotstar and install
- search for WifiManager, scroll down and install the one with the exact title
- search for ArduinoJSON and install

To upload the code you want to go through the following steps:
- click Tools -> Boards and select "Generic ESP8266 module"
- click Tools -> Port  and select the right COM port of your device
- click the arrow facing right to upload the code!


Enjoy Hacking!

